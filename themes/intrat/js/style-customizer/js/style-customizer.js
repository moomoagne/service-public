/* @author Tigran Sargsyan <tigran.sn@gmail.com> */

/* jshint globalstrict: true, browser: true, jquery: true */
/* global less */

'use strict';
var bg_color = '';
var bg_img = '';
var colorPrimary = '';
var colorSecondary = '';
var layout = '';
var bgLayout = '';
if (jQuery('#matri-color').length){
	colorPrimary = jQuery('#matri-color').data('colorprimary');
}
if (jQuery('#matri-color').length){
	colorSecondary = jQuery('#matri-color').data('colorsecondary');
}
if(jQuery('#matri-layout').length){
	layout = jQuery('#matri-layout').data('layout');
}
if(jQuery('#matri-layout').length){
	bgLayout = jQuery('#matri-layout').data('bgtype');
}
if (jQuery('#matri-bgcolor').length){
	bg_color = jQuery('#matri-bgcolor').data('bgcolor');
}
if (jQuery('#matri-bgimg').length){
 	bg_img = jQuery('#matri-bgimg').data('bgimg');
}

less.modifyVars({
  '@colorPrimary'	: colorPrimary,
  '@colorSecondary'	: colorSecondary,
  '@layoutType'		: '"' + layout + '"',
  '@outerBgType' 	: '"' + bgLayout + '"',
  '@outerBgColor'	: bg_color ,
  '@outerBgImage'	:'"' +  bg_img +'"'
});

// Less variables
var lessVars = {
	'colorPrimary': colorPrimary,
	'colorSecondary': colorSecondary,
	'colorTertiary': '#38cef9',
	'colorBg': '#ffffff',
	'layoutType': layout ,
	'borderType': 'square',
	'boxWidth': '1240px',
	'outerBgType':  bgLayout ,
	'outerBgColor':  bg_color ,
	'outerBgImage':   bg_img ,
	'preloader': '',
};



var styleCustomizer = {
	files: {

	},

	changeVar: function(key, value, el) {
		switch(key) {
		case 'outerBgType':
			if(!this.files.outerBgImage) {
				if(value === 'pattern') {
					lessVars.outerBgImage = bg_img;
				} else if (value === 'image') {
					lessVars.outerBgImage = bg_img;
				}
			}
			break;
		case 'outerBgImage':
			if(el.files.length) {
				this.files.outerBgImage = el.files[0];
				value = URL.createObjectURL(el.files[0]);
			} else {
				return;
			}
			break;
		}

		lessVars[key] = value;
		this.changeVars(lessVars);
		this.onVarChange(key, value, el);
	},

	changeVars: function(vars) {
		var quotedVars = [
			'outerBgImage',
			'layoutType',
			'borderType',
			'outerBgType',
			'preloader'
		];
		vars = jQuery.extend({}, vars);
		quotedVars.forEach(function(key) {
			vars[key] = '"' + vars[key] + '"';
		});
		less.modifyVars(vars);
	},

	onVarChange: function(key, value, element) {},

	downloadCssFile: function() {
		var link, file;

		file = new Blob([this._getCssText()], {type: 'text/css'});
		link = document.createElement('a');
		link.href = URL.createObjectURL(file);
		link.style.display = 'none';
		link.download = 'skin.css';
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);

		jQuery('.sc-after-save-todo-point-image').toggle(lessVars.layoutType === 'boxed' && !!this.files.outerBgImage);
		jQuery('.sc-after-save-todo-point-preloader').toggle(lessVars.preloader !== '');
		jQuery('#afterSaveCSSFileModal').modal('show');
	},

	_getCssText: function() {
		var css;

		css = document.querySelector('style[id^="less:"]');
		if(!css) {
			alert('Error: css file not generated');
			throw new Error('Error: css file not generated');
		}
		css = css.innerHTML;
		if(this.files.outerBgImage) {
			css = css.replace(/url\("blob:[^"]+"\)/, 'url("' + bg_img + '")');
		}
		return css;
	}

};


// Listen to variable controls changes
jQuery('.sc-variable').change(function(e) {
	styleCustomizer.changeVar(this.dataset.key, this.value, this);
});

// Toggle styles customizer
jQuery('#sc-toggle').click(function(e) {
	jQuery('#style-customizer').toggleClass('expanded');
});

if(navigator.userAgent.indexOf('Trident') !== -1) {
	jQuery('#sc-download-css').after('<p><strong class="text-danger">File download is not supported in Internet Explorer.</strong></p>');
}
jQuery('#sc-download-css').click(function(e) {
	styleCustomizer.downloadCssFile();
});

// Fill in the variables into controls
jQuery('.sc-variable').each(function() {
	if(this.dataset.key === 'outerBgImage') return;

	if(this.type === 'radio') {
		this.checked = this.value === lessVars[this.dataset.key];
	} else {
		this.value = lessVars[this.dataset.key];
	}
});


styleCustomizer.onVarChange = function(key, value, el) {
	var file, preloader_html;

	switch(key) {
	case 'layoutType':
		jQuery(window).trigger('resize');
		jQuery('#outer-bg-section').toggle(value === 'boxed');
		break;

	case 'outerBgType':
		if(value === 'color') {
			jQuery('#sc-bg-outer-color-wrap').show();
			jQuery('#sc-bg-outer-image-wrap').hide();
		} else {
			jQuery('#sc-bg-outer-color-wrap').hide();
			jQuery('#sc-bg-outer-image-wrap').show();
		}
		break;

	case 'preloader':
		preloader_html = value ? '<div class="preloader" aria-label="Loading">' + jQuery.trim(jQuery('#sc-preloader-wrap-' + value).html().replace(/@sample@/g, '')) + '</div>' : '';
		jQuery('.preloader-wrap').html(preloader_html);
		if(!value) break;
		jQuery('.sc-preloader-html').text(preloader_html);
		jQuery('body').addClass('loading');
		setTimeout(function() {
			jQuery('body').removeClass('loading');
		}, 1200);
		break;
	}
};
