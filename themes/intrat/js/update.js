(function ($) {
	$('.parallax').parallaxBackground();

	$(window).on('load', function(){ 
		setTimeout(function(){
	    	$('.loader-live').fadeOut();
	  	},1000);
	});

	// Get all elements with class="closebtn"
	var close = document.getElementsByClassName("alert-closebtn");
	var i;

	// Loop through all close buttons
	for (i = 0; i < close.length; i++) {
	    // When someone clicks on a close button
	    close[i].onclick = function(){

	        // Get the parent of <span class="closebtn"> (<div class="alert">)
	        var div = this.parentElement;

	        // Set the opacity of div to 0 (transparent)
	        div.style.opacity = "0";

	        // Hide the div after 600ms (the same amount of milliseconds it takes to fade out)
	        setTimeout(function(){ div.style.display = "none"; }, 600);
	    }
	}

	$('.parallax').parallaxBackground();
  /*	if($('.counter').length){
		$('.counter').counterUp({
		    delay: 10,
		    time: 5000
		});
	}
  */
/*$(document).ready(function(){
    incrementalNumber();
  });*/


    $('#datetimepicker4').datetimepicker();

    $('#datetimepicker3').datetimepicker();

    $('#datetimepicker1').datetimepicker();
      autoclose: true;

    $('#datetimepicker5').datetimepicker({
        defaultDate: "11/1/2016",
        disabledDates: [
            moment("12/25/2016"),
            new Date(2016, 11 - 1, 21),
            "07/22/2016 00:53"
        ]
    });

    $('#datetimepicker2').datetimepicker({
        format: 'LT'
    });

    $('#datetimepicker9').datetimepicker({
        viewMode: 'years'
    });

 	$('#datetimepicker12').datetimepicker({
    	inline: true,
	});

  $('#datetimepicker13').datetimepicker({
      inline: true,
  });
  $(document).ready(function () {                                  
    $('#datetimepicker4').datetimepicker({
        format: "dd/mm/yyyy",
        autoclose: true
    });
  });  

  $(document).ready(function(){
    incrementalNumber();
  });
  $( ".clocktime" ).each(function() {
    var date_time = $(this).data('time');
    $(this).countdown(date_time).on('update.countdown', function(event) {
    var $this = $(this).html(event.strftime(''
      + '<p><span>%-w</span> <b>week%!w</b></p> '
      + '<p><span>%-d</span> <b>day%!d</b></p> '
      + '<p><span>%H</span> <b>hr </b></p>'
      + '<p><span>%M</span> <b>min</b></p> '
      + '<p><span>%S</span> <b>sec</b></p>'));
    });
  });
  //coming soon
  $( ".clock-coming-soon" ).each(function() {
    var date_time = $(this).data('time');
    $(this).countdown(date_time).on('update.countdown', function(event) {
     var $this = $(this).html(event.strftime(''
       + '<p><span>%-w</span> <b>week%!w</b></p> '
       + '<p><span>%-d</span> <b>day%!d</b></p> '
       + '<p><span>%H</span> <b>hr </b></p>'
       + '<p><span>%M</span> <b>min</b></p> '
       + '<p><span>%S</span> <b>sec</b></p>'));
     });
   });

  /*$('#js-grid-mosaic-projects .cbp-wrapper').each(function(){
    var thiss = $(this);
   */ /*$('#js-grid-mosaic-projects .cbp-item:nth-of-type( 3)').each(function(){
      var data_img1 = $(this).find('.masonry-image').data('image1');
      var data_img2 = $(this).find('.masonry-image').data('image2');


      $(this).find('.masonry-image').append("<img src="+ data_img1 +" alt='' class='image1'>");
      
    });*/
  /*});*/



//one page1
$(window).scroll(function () {
     var sc = $(window).scrollTop()
    if (sc > 180) {
        $("header.oneheader").addClass("small")
    } else {
        $("header.oneheader").removeClass("small")
    }
});

$(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
  
  $("div.bhoechie-tab-menu2>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab2>div.bhoechie-tab-content2").removeClass("active");
        $("div.bhoechie-tab2>div.bhoechie-tab-content2").eq(index).addClass("active");
    });
  
});


$(document).ready(function(){
    $('.product_preview_left .previews a').click(function(){
      var largeImage = $(this).attr('data-full');
      $('.selected').removeClass();
      $(this).addClass('selected');
      $('.full img').hide();
      $('.full img').attr('src', largeImage);
      $('.full img').fadeIn();


    }); // closing the listening on a click
    $('.full img').on('click', function(){
      var modalImage = $(this).attr('src');
      $.fancybox.open(modalImage);
    });
  }); //closing our doc ready

})(jQuery);

/* popup */
/*document.addEventListener("DOMContentLoaded", function () {

  // Select your overlay trigger
  var trigger = document.querySelector('#js-overlay-trigger');
  
  trigger.addEventListener('click', function(e){
  e.preventDefault();

  novicell.overlay.create({
    'selector': trigger.getAttribute('data-element'),
    'class': 'selector-overlay',
    "onCreate": function() { console.log('created'); },
    "onLoaded": function() { console.log('loaded'); },
    "onDestroy": function() { console.log('Destroyed'); }
  });
  });

  // Video overlay
  var videoOverlayTriggers = document.querySelectorAll('.js-video-overlay-trigger');

  for (var i = 0; i < videoOverlayTriggers.length; i++) {
  videoOverlayTriggers[i].addEventListener('click', function(e){
    e.preventDefault();

    var currentTrigger = e.target;

    novicell.overlay.create({
    'videoId': currentTrigger.getAttribute('data-video-id'),
    'type': currentTrigger.getAttribute('data-type'),
    'class': 'video-overlay'
    });


  });
  }

});*/

var ctx = document.getElementById("myChart");
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: ["January", "February", "March", "April", "May", "June", "July"],
              datasets: [{
                  label: "My First dataset",
                  backgroundColor: "rgba(255,99,132,0.2)",
                  borderColor: "rgba(255,99,132,1)",
                  borderWidth: 1,
                  hoverBackgroundColor: "rgba(255,99,132,0.4)",
                  hoverBorderColor: "rgba(255,99,132,1)",
                  data: [65, 59, 80, 81, 56, 55, 40],
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero:true
                      }
                  }]
              }
          }
      });

      var ctx = document.getElementById("myLineChart");
      var myLineChart = Chart.Line(ctx, {
          type: 'line',
          data: {
              labels: ["January", "February", "March", "April", "May", "June", "July"],
             datasets: [
              {
                  label: "My First dataset",
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(75,192,192,0.4)",
                  borderColor: "rgba(75,192,192,1)",
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  pointBorderColor: "rgba(75,192,192,1)",
                  pointBackgroundColor: "#fff",
                  pointBorderWidth: 1,
                  pointHoverRadius: 5,
                  pointHoverBackgroundColor: "rgba(75,192,192,1)",
                  pointHoverBorderColor: "rgba(220,220,220,1)",
                  pointHoverBorderWidth: 2,
                  pointRadius: 1,
                  pointHitRadius: 10,
                  data: [65, 59, 80, 81, 56, 55, 40],
              }
          ]
          },
          
      });

      var ctx = document.getElementById("myRadarChart");
      var myRadarChart = new Chart(ctx, {
          type: 'radar',
          data: {
              labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
             datasets: [
              {
                  label: "My First dataset",
                  backgroundColor: "rgba(179,181,198,0.2)",
                  borderColor: "rgba(179,181,198,1)",
                  pointBackgroundColor: "rgba(179,181,198,1)",
                  pointBorderColor: "#fff",
                  pointHoverBackgroundColor: "#fff",
                  pointHoverBorderColor: "rgba(179,181,198,1)",
                  data: [65, 59, 90, 81, 56, 55, 40]
              },
              {
                  label: "My Second dataset",
                  backgroundColor: "rgba(255,99,132,0.2)",
                  borderColor: "rgba(255,99,132,1)",
                  pointBackgroundColor: "rgba(255,99,132,1)",
                  pointBorderColor: "#fff",
                  pointHoverBackgroundColor: "#fff",
                  pointHoverBorderColor: "rgba(255,99,132,1)",
                  data: [28, 48, 40, 19, 96, 27, 100]
              }
          ]
          },
          
      });
      
      var ctx = document.getElementById("myPieChart");
      var myPieChart = new Chart(ctx,{
          type: 'pie',
          data: {
             labels: [
              "Red",
              "Blue",
              "Yellow"
          ],
             datasets: [
              {
                  data: [300, 50, 100],
                  backgroundColor: [
                      "#FF6384",
                      "#36A2EB",
                      "#FFCE56"
                  ],
                  hoverBackgroundColor: [
                      "#FF6384",
                      "#36A2EB",
                      "#FFCE56"
                  ]
              }]
          },
          
      });