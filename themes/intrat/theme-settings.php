<?php

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Form\ThemeSettingsForm;
use Drupal\file\Entity\File;
use Drupal\Core\Url;

function intrat_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $theme_file = drupal_get_path('theme', 'intrat') . '/theme-settings.php';
    $build_info = $form_state->getBuildInfo();
    if (!in_array($theme_file, $build_info['files'])) {
        $build_info['files'][] = $theme_file;
    }
    $form_state->setBuildInfo($build_info);

    $form['#submit'][] = 'intrat_theme_settings_form_submit';


        $form['settings'] = array(
        '#type' => 'details',
        '#title' => t('Theme settings'),
        '#open' => TRUE,
        '#attached' => array(
          'library' =>  array(
            'intrat/theme-color-lib'
          ),
        ),
    );







    $form['settings']['general_setting'] = array(
        '#type' => 'details',
        '#title' => t('General Settings'),
        '#open' => FALSE,
    );

    $form['settings']['general_setting']['general_setting_tracking_code'] = array(
        '#type' => 'textarea',
        '#title' => t('Tracking Code'),
        '#default_value' => theme_get_setting('general_setting_tracking_code', 'intrat'),
    );
    /*$form['settings']['general_setting']['welcome_message'] = array(
        '#type' => 'textfield',
        '#title' => t('Welcome Message'),
        '#attributes' => array(
            'class' => array('colorpickerField'), // insert space before attribute name :)
        ),
        '#default_value' => theme_get_setting('welcome_message', 'intrat'),
    );*/
    /* $form['settings']['general_setting']['operating_hours'] = array(
        '#type' => 'textfield',
        '#title' => t('Operating hours'),
        '#default_value' => theme_get_setting('operating_hours', 'intrat'),
    );
    $form['settings']['general_setting']['social_network'] = array(
        '#type' => 'textarea',
        '#title' => t('Social Network'),
        '#default_value' => theme_get_setting('social_network', 'intrat'),
    );
    $form['settings']['general_setting']['location'] = array(
        '#type' => 'textfield',
        '#title' => t('Location'),
        '#default_value' => theme_get_setting('location', 'intrat'),
    );
    $form['settings']['general_setting']['email'] = array(
        '#type' => 'textfield',
        '#title' => t('Email'),
        '#default_value' => theme_get_setting('email', 'intrat'),
    );
    $form['settings']['general_setting']['phone'] = array(
        '#type' => 'textfield',
        '#title' => t('Phone'),
        '#default_value' => theme_get_setting('phone', 'intrat'),
    );
    $form['settings']['general_setting']['link_button'] = array(
        '#type' => 'textfield',
        '#title' => t('Link Button'),
        '#default_value' => theme_get_setting('link_button', 'intrat'),
    );*/
    // footer settings
    // $form['settings']['general_setting']['footer'] = array(
    //     '#type' => 'details',
    //     '#title' => t('Footer settings'),
    //     '#open' => FALSE,
    // );


    // $form['settings']['general_setting']['footer']['copyright_text'] = array(
    //     '#type' => 'textarea',
    //     '#title' => t('Copyright text'),
    //     '#default_value' => theme_get_setting('copyright_text', 'intrat'),
    // );
    

// Header settings
    $form['settings']['header'] = array(
        '#type' => 'details',
        '#title' => t('Header settings'),
        '#open' => FALSE,
    );
    

    $form['settings']['header']['header_layout'] = array(
        '#type' => 'select',
        '#title' => t('Header default sidebar'),
        '#options' => array(
            'default' => t('Default'),
            'header2' => t('Header 2'),
            'header3' => t('Header 3'),
        ),
        '#required' => true,
        '#default_value' => theme_get_setting('header_layout', 'intrat'),
    );


// Footer settings
    $form['settings']['footer'] = array(
        '#type' => 'details',
        '#title' => t('Footer settings'),
        '#open' => FALSE,
    );
    

    $form['settings']['footer']['footer_layout'] = array(
        '#type' => 'select',
        '#title' => t('Header default sidebar'),
        '#options' => array(
            'default' => t('Default'),
            'footer2' => t('Footer 2'),
            'footer3' => t('Footer 3')
        ),
        '#required' => true,
        '#default_value' => theme_get_setting('footer_layout', 'intrat'),
    );

    $form['settings']['footer']['copyright'] = array(
        '#type' => 'textfield',
        '#title' => t('Copyright'),
        '#default_value' => theme_get_setting('copyright', 'intrat'),
    );
    $form['settings']['footer']['social_network'] = array(
        '#type' => 'textarea',
        '#title' => t('Social Network'),
        '#default_value' => theme_get_setting('social_network', 'intrat'),
    );

// page settings
    $form['settings']['page_title'] = array(
        '#type' => 'details',
        '#title' => t('Page title settings'),
        '#open' => FALSE,
    );
    $form['settings']['page_title']['page_title_layout'] = array(
        '#type' => 'select',
        '#title' => t('Page title Layout'),
        '#options' => array(
            'page_title1' => t('Layout: Page title 1'),
            'page_title2' => t('Layout: Page title 2'),
            
        ),
        '#required' => true,
        '#default_value' => theme_get_setting('page_title_layout', 'intrat'),
    );
    $form['settings']['page_title']['default_breadcrumb_image'] = array(
        '#type'     => 'managed_file',
        '#title'    => t('Page title background image upload'),
        '#required' => FALSE,
        '#upload_location' => 'public://background/',
        '#default_value' => theme_get_setting('default_breadcrumb_image','intrat'),
        '#upload_validators' => array(
            'file_validate_extensions' => array('gif png jpg jpeg'),
            '#progress_message' => 'Uploading ...',
            '#required' => FALSE,
        ),
    );

// Blog settings
    $form['settings']['blog'] = array(
        '#type' => 'details',
        '#title' => t('Blog settings'),
        '#open' => FALSE,
    );


    $form['settings']['blog']['blog_breadcrumb_image'] = array(
        '#type'     => 'managed_file',
        '#title'    => t('Page title background image upload'),
        '#required' => FALSE,
        '#upload_location' => 'public://background/',
        '#default_value' => theme_get_setting('blog_breadcrumb_image','intrat'),
        '#upload_validators' => array(
            'file_validate_extensions' => array('gif png jpg jpeg'),
            '#progress_message' => 'Uploading ...',
            '#required' => FALSE,
        ),
    );

    $form['settings']['blog']['blog_layout'] = array(
        '#type' => 'select',
        '#title' => t('Blog Layout'),
        '#options' => array(
            'classic' => t('Classic layout'),
            'default' => t('Default layout'),
            'thumbnail'=> t('Thumbnail layout'),
            'one_column'=>t('One column grid'),
            'two_column'=>t('Two column grid'),
            'three_column'=>t('Three column grid'),
            'four_column'=>t('Four column grid'),
            'five_column'=>t('Five column grid'),
            'six_column'=>t('Six column grid')
        ),
        '#required' => true,
        '#default_value' => theme_get_setting('blog_layout', 'intrat'),
    );

    $form['settings']['blog']['blog_sidebar'] = array(
        '#type' => 'select',
        '#title' => t('Blog list default sidebar'),
        '#options' => array(
            'left' => t('Left'),
            'right' => t('Right'),
            'full_width' => t('Full width'),
        ),
        '#required' => true,
        '#default_value' => theme_get_setting('blog_sidebar', 'intrat'),
    );

// Blog single settings
    $form['settings']['blog']['blog_single'] = array(
        '#type' => 'details',
        '#title' => t('Blog single settings'),
        '#open' => FALSE,
    );

    $form['settings']['blog']['blog_single']['blog_single_sidebar'] = array(
        '#type' => 'select',
        '#title' => t('Default sidebar'),
        '#options'  => array(
            'left'  => t('Left'),
            'right' => t('Right'),
            'full_width' => t('Full width'),
           
        ),

        '#default_value' => theme_get_setting('blog_single_sidebar', 'intrat'),
    );


// Shop settings
    $form['settings']['shop'] = array(
        '#type' => 'details',
        '#title' => t('Shop settings'),
        '#open' => FALSE,
    );


    $form['settings']['shop']['shop_breadcrumb_image'] = array(
        '#type'     => 'managed_file',
        '#title'    => t('Page title background image upload'),
        '#required' => FALSE,
        '#upload_location' => 'public://background/',
        '#default_value' => theme_get_setting('shop_breadcrumb_image','intrat'),
        '#upload_validators' => array(
            'file_validate_extensions' => array('gif png jpg jpeg'),
            '#progress_message' => 'Uploading ...',
            '#required' => FALSE,
        ),
    );
    $form['settings']['shop']['shop_single_sidebar'] = array(
        '#type' => 'select',
        '#title' => t('Shop Single Sidebar'),
        '#options'  => array(
            'left'  => t('Left'),
            'right' => t('Right'),
            'both' => t('both'),
            'full_width' => t('Full width'),
           
        ),
        '#default_value' => theme_get_setting('shop_single_sidebar', 'intrat'),
    );
    $form['settings']['shop']['shop_gird_column'] = array(
        '#type' => 'select',
        '#title' => t('Shop Gird Column'),
        '#options'  => array(
            '2col'  => t('2 Column'),
            '3col' => t('3 column'),
            '4col' => t('4 column'),
            '5col' => t('5 column'),
            '6col' => t('6 column'),
           
        ),
        '#default_value' => theme_get_setting('shop_gird_column', 'intrat'),
    );


// Contact settings
    $form['settings']['contact'] = array(
        '#type' => 'details',
        '#title' => t('Contact settings'),
        '#open' => FALSE,
    );


    $form['settings']['contact']['contact_body'] = array(
        '#type' => 'textarea',
        '#title' => t('Content'),
        '#default_value' => theme_get_setting('contact_body', 'intrat'),
    );

    $form['settings']['contact']['contact_breadcrumb_image'] = array(
        '#type'     => 'managed_file',
        '#title'    => t('Page title background image upload'),
        '#required' => FALSE,
        '#upload_location' => 'public://background/',
        '#default_value' => theme_get_setting('contact_breadcrumb_image','intrat'),
        '#upload_validators' => array(
            'file_validate_extensions' => array('gif png jpg jpeg'),
            '#progress_message' => 'Uploading ...',
            '#required' => FALSE,
        ),
    );

    $form['settings']['contact']['contact_page_layouts'] = array(
        '#type' => 'select',
        '#title' => t('Contact page layouts'),
        '#options'  => array(
            'parallax'      => t('Contact parallax'),
            'left_sidebar'  => t('Contact left sidebar'),
            'right_sidebar' => t('Contact right sidebar'),
            'map'           => t('Contact map'),
            'classic'       => t('Contact classic')
        ),
        '#default_value' => theme_get_setting('contact_page_layouts', 'intrat'),
    );
    /*$form['settings']['contact']['address'] = array(
        '#type' => 'textfield',
        '#title' => t('Address name'),
        '#default_value' => theme_get_setting('address','intrat')
    );

    $form['settings']['contact']['contact_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Contact form title'),
        '#default_value' => theme_get_setting('contact_title','intrat')
    );
*/
    $form['settings']['contact']['contact_address_info'] = array(
        '#type' => 'textarea',
        '#title' => t('Contact address info'),
        '#default_value' => theme_get_setting('contact_address_info', 'intrat'),
    );


// custom css
    $form['settings']['custom_css'] = array(
        '#type' => 'details',
        '#title' => t('Custom CSS'),
        '#open' => FALSE,
    );

    $form['settings']['custom_css']['custom_css'] = array(
        '#type' => 'textarea',
        '#title' => t('Custom CSS'),
        '#default_value' => theme_get_setting('custom_css', 'intrat'),
        '#description' => t('<strong>Example:</strong><br/>h1 { font-family: \'Metrophobic\', Arial, serif; font-weight: 400; }')
    );



}

function intrat_theme_settings_form_submit(&$form, FormStateInterface $form_state) {
    $account = \Drupal::currentUser();
    $values = $form_state->getValues();

    $bg[0] = $values['default_breadcrumb_image']; 
    $bg[1] = $values['blog_breadcrumb_image']; 
    $bg[2] = $values['contact_breadcrumb_image'];
    $bg[3] = $values['background_image'];
    $bg[4] = $values['shop_breadcrumb_image'];
    // $bg[4] = $values['default_contact_breadcrumb_image'];
    // /*$bg[4] = $values['port_title_bg'];*/

    $count = count($bg);
    for ($i=0; $i < $count; $i++) {

    if (isset($bg[$i]) && !empty($bg[$i])) {
          // Load the file via file.fid.
          $file1 = file_load($bg[$i][0]);
          // Change status to permanent.
          $file1->setPermanent();
          $file1->save();
          $file_usage = \Drupal::service('file.usage');
          $file_usage->add($file1, 'intrat', 'theme', 1);
        } 
    }
        
}
?>